/**
 	@author: Deepak Mishra
	@since: September 05, 2015
	Class: Main
	@return: Execute the classes inside of AddresBookApplication package
	@param: Main Method  
**/

package AddressBookApplication;

import java.io.IOException;

public class AddressBookApplication 
{
	
	// Main method used to execute classes inside of AddressBookApplication
	public static void main(String[] args) throws IOException 
	{
	String Input;
	AddressBook TheAddressBook = new AddressBook();
	Menu TheMenu = new Menu(TheAddressBook);
	do
	{	
		Input = TheMenu.PrintMenu();
		TheMenu.takeInput(Input);
	}while(Input.equals("f") == false);
		

	}

}