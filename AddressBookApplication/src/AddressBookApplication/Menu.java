/**
 	@author: Deepak Mishra
	@since: September 05, 2015
	Class: Menu
	@return: Used to display and execute the users options 
	@param: void LoadEnteries(),AddNewEntry(),RemoveEntry(),void Find(),void Listing(),void SaveAndQuit()
**/
/*
	Menu- this class is used to display menu options to the user. This includes
a) Loading of entries from a file.
b) Addition - prompting user for information to be used to create a new AddressEntry
c) Removal - removing of an AddressEntry from the AddressBook. First a find is done (see below example session) and then the user selects from the find results what entry to remove.
d) Find - prompts users for the begining of the users last name (they can enter a complete last name or just the start....all entries with that as the first part of their last name will be displayed). Note in the case when more then one entry is found a set will be returned.
e) Listing - listing (a "dump") of the addresses in alphabetic order by the person's last name.
f) Save and Quit - here the user gives name of file where entire current (in memory) AddressBook is dumped --in format that can be read in using option a!!!
*/

package AddressBookApplication;

import java.io.IOException;
import java.util.Scanner;

public class Menu 
{	
	Scanner in = new Scanner(System.in);
	AddressBook menuAddressBook = new AddressBook();
	
	Menu(AddressBook theAddressBook)
	{
		this.menuAddressBook = theAddressBook;

	}
	
	String PrintMenu()
	{
		
		System.out.println("a) Loading of entries from a file.");
		System.out.println("b) Addition ");
		System.out.println("c) Removal ");
		System.out.println("d) Find ");
		System.out.println("e) Listing ");
		System.out.println("f) Save and Quit");
		return(in.nextLine());
	}
	
	
	void takeInput(String UserInput) throws IOException
	{
		
		String[] Input = new String[9];;
		Input[8] = UserInput;
		if (Input[8].equals("a"))
		{
			System.out.println("Enter a file path to load the file from.");
			Input[0] = in.nextLine();
			menuAddressBook.readFile(Input[0]);
		}
		else if (Input[8].equals("b"))
		{
			System.out.println("Enter First Name");
			Input[0] = in.nextLine();
			while(Input[0].isEmpty())
			{
				System.out.println("Enter First Name");
				Input[0] = in.nextLine();
			}
			System.out.println("Enter Last Name");
			Input[1] = in.nextLine();
			while(Input[1].isEmpty())
			{
				System.out.println("Enter Last Name");
				Input[1] = in.nextLine();
			}
			System.out.println("Enter Street Name");
			Input[2] = in.nextLine();
			while(Input[2].isEmpty())
			{
				System.out.println("Enter Street Name");
				Input[2] = in.nextLine();
			}
			System.out.println("Enter City Name");
			Input[3] = in.nextLine();
			while(Input[3].isEmpty())
			{
				System.out.println("Enter City Name");
				Input[3] = in.nextLine();
			}
			System.out.println("Enter State Name");
			Input[4] = in.nextLine();
			while(Input[4].isEmpty())
			{
				System.out.println("Enter State Name");
				Input[4] = in.nextLine();
			}
			String regex = "\\d+";
			System.out.println("Enter Zip Code");
			Input[5] = in.nextLine();
			while(Input[5].isEmpty() || Input[5].matches(regex)==false )
			{
				System.out.println("Enter Zip Code");
				Input[5] = in.nextLine();
			}
			String[] segments;
			String[] segmentss = new String[1];
			do{
				System.out.println("Enter Email");
				Input[6] = in.nextLine();
				String email =  Input[6];
				segments = email.split("@");
				if(segments[0].isEmpty() || segments.length != 2 || email.contains("@.") || (segments[0].contains("//.") && segments[1].contains("@") ))
				{
					continue;
				}
				segmentss = segments[1].split("\\.");
				if(segmentss.length >= 2)
				{
					continue;
					
				}
				if(segmentss[0].isEmpty() || segmentss[1].isEmpty())
				{
					continue;
				}
			}while(Input[6].isEmpty() || segments.length != 2 || segmentss.length < 2 );
			System.out.println("Enter Phone");
			Input[7] = in.nextLine();
			while(Input[7].isEmpty() || Input[7].matches(regex)==false)
			{
				System.out.println("Enter Phone");
				Input[7] = in.nextLine();
			}
			menuAddressBook.add(new AddressEntry(Input[0],Input[1],Input[2],Input[3],Input[4],Input[5],Input[6],Input[7]));
		}
		else if (Input[8].equals("c"))
		{
			System.out.println("Enter Last Name");
			Input[0] = in.nextLine();
			while(Input[0].isEmpty())
			{
				System.out.println("Enter Last Name");
				Input[0] = in.nextLine().trim();
			}
			menuAddressBook.remove(Input[0]);
		}
		else if (Input[8].equals("d"))
		{
			System.out.println("Enter Last Name");
			Input[0] = in.nextLine();
			while(Input[0].isEmpty())
			{
				System.out.println("Enter Last Name");
				Input[0] = in.nextLine().trim();
			}
			for(AddressEntry Address : menuAddressBook.find(Input[0]))
			{
				
				System.out.print(menuAddressBook.AddressBookList.indexOf(Address));
				System.out.print(Address.toString());
			}
		}
		else if (Input[8].equals("e"))
		{
			menuAddressBook.list();
		}
		else if (Input[8].equals("f"))
		{
			System.out.println("Enter a file path to write the file to.");
			Input[0] = in.nextLine();
			while(Input[0].isEmpty())
			{
				System.out.println("Enter a file path to write the file to.");
				Input[0] = in.nextLine().trim();
			}
			menuAddressBook.toFile(Input[0]);
			System.out.println("Saved. Goodbye.");
		}
		
	}

	
}
