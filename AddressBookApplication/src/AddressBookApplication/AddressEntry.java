/**
	@author: Deepak Mishra
	@since : September 05, 2015
	Class: AddressEntry
	@param: These individual AddressEntry Objects, keep data of One Person, and is used to fill up the AddressBook Object 
	Contains: 
 	@return
 **/
 
/*
AddressEntry - this class represents a single Address/Contact information entry in the AddressBook
It contains a contructor that accepts a first, last name, street,city, state, zip, email, phone. 
It contains separate class varaibles representing all the information in 
an AddressEntry contact.It contains a toString() method that returns a nicely formats a string 
containing all contact information for printing to console.It contains a toFile() method that
returns a nicely formated string containing all the contact information for saving to a file.It 
contain setX() and getX() methods where X are the class variables.
 */
package AddressBookApplication;

public class AddressEntry 
{
	String first;
	String last;
	String street;
	String city;
	String state;
	String zip;
	String email;
	String phone;
	
	/* 
		Usage: AddressEntry(FirstName, LastName,Street,City, State, Zip, Email, Phone)
		Details:AddressEntry Object Constructor. All the parameters are Strings. 
	*/
	AddressEntry(String first, String last, String street, String city, String state, String zip, String email,String phone) 
	{
		this.first = first;
		this.last = last;
		this.street = street;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.email = email;
		this.phone = phone;
	}

	
	/* 
	Usage: AddressEntry.toString();
	Details: Returns a string with all the AddressEntry fields. 
	 */
	@Override
	public String toString() 
	{
		return "	"+first + " " + last + "\r\n	" + street + "\r\n	" + city + "," + state +" "+ zip  + "\r\n	" + email  + "\r\n	" + phone +"\r\n\r\n";
	}
	
	/* 
	Usage: AddressEntry.toFile();
	Details: Returns a string with all the AddressEntry fields. 
	 */	
	String toFile() 
	{
		return first + "\r\n" + last + "\r\n" + street + "\r\n" + city + "\r\n" + state +"\r\n"+ zip  + "\r\n" + email  + "\r\n" + phone+"\r\n";
	}
	
	/* 
	Usage: AddressEntry.getFirst();
	Details: Returns First Name as a String. 
	 */
	String getFirst() 
	{
		return first;
	}

	/* 
	Usage: AddressEntry.setFirst(Name);
	Details: Sets First Name as String Name. 
	 */
	void setFirst(String first) 
	{
		this.first = first;
	}

	/* 
	Usage: AddressEntry.getLast();
	Details: Returns Last Name as a String. 
	 */
	String getLast() 
	{
		return last;
	}
	


	/* 
	Usage: AddressEntry.setLast(Name);
	Details: Sets Last Name as String Name. 
	 */
	void setLast(String last) 
	{
		this.last = last;
	}

	/* 
	Usage: AddressEntry.getLast();
	Details: Returns Street as a String. 
	 */
	String getStreet() 
	{
		return street;
	}

	/* 
	Usage: AddressEntry.setStreet(Name)
	Details: Sets Street as String Name. 
	 */
	void setStreet(String street) 
	{
		this.street = street;
	}

	/* 
	Usage: AddressEntry.getCity();
	Details: Returns City as a String. 
	 */
	String getCity() 
	{
		return city;
	}

	/* 
	Usage: AddressEntry.setCity(Name)
	Details: Sets City as String Name. 
	 */
	void setCity(String city)
	{
		this.city = city;
	}

	/* 
	Usage: AddressEntry.getState();
	Details: Returns State as a String. 
	 */
	String getState() 
	{
		return state;
	}

	/* 
	Usage: AddressEntry.setState(Name)
	Details: Sets State as String Name. 
	 */
	void setState(String state) 
	{
		this.state = state;
	}

	/* 
	Usage: AddressEntry.getZip();
	Details: Returns Zip as a String. 
	 */
	String getZip() 
	{
		return zip;
	}

	/* 
	Usage: AddressEntry.setZip(Name)
	Details: Sets Zip as String Name. 
	 */
	void setZip(String zip) 
	{
		this.zip = zip;
	}

	/* 
	Usage: AddressEntry.getEmail();
	Details: Returns Email as a String. 
	 */
	String getEmail() 
	{
		return email;
	}

	/* 
	Usage: AddressEntry.setEmail(Name)
	Details: Sets Email as String Name. 
	 */
	void setEmail(String email) 
	{
		this.email = email;
	}

	/* 
	Usage: AddressEntry.getPhone();
	Details: Returns Phone Number as a String. 
	 */
	String getPhone() 
	{
		return phone;
	}

	/* 
	Usage: AddressEntry.setPhone(Name)
	Details: Sets Phone Number as String Name. 
	 */
	void setPhone(String phone) 
	{
		this.phone = phone;
	}


	






	
}
