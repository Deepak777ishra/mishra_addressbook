/**
    @author : Deepak Mishra
	@since: September 05, 2015
	Class: AddressBook
	@param: AddressBook Object is an ArrayList of AddressEntry objects and is used to organize the AddressEntries. 
	@return: 

**/

/*
AddressBook - this class represents and contains a possibly every growing and/or shrinking "list" 
of AddressEntries. It allows for various operations such as search/find, addition and removal of 
AddressEntries. The followiing functionality is supported: lists alphabetic order (sorted), add, 
remove, find (search), store to file , read in from file.
 */


package AddressBookApplication;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.hamcrest.Matcher;

public class AddressBook 
{
	ArrayList<AddressEntry> AddressBookList = new ArrayList<AddressEntry>();


	/* 
	Usage: AddressBook.list()
	Details:Prints out the Address Entry objects inside AddressBook object sorted by last name.
	 */
	void list()
	{
		Collections.sort(AddressBookList, new Comparator<AddressEntry>(){
		    public int compare(AddressEntry a,AddressEntry b){
		        return a.getLast().compareTo(b.getLast());
		        // or compare what you want return -1, 0 or 1 
		        // for less than, equal and greater than resp.
		    }
		});
		
		for(AddressEntry Address : AddressBookList)
		{
			
			System.out.print(AddressBookList.indexOf(Address));
			System.out.print(Address.toString());
		}
	}
	
	
	
	/* 
	Usage: AddressBook.add(Address)
	Details:Adds the Address AddressEntry object to the AddressBook Object.
	 */
	void add(AddressEntry Address)
	{
		AddressBookList.add(Address);

	}
	
	
	
	
	/* 
	Usage: AddressBook.remove(LastName)
	Details:Prints out the Address Entry objects inside AddressBook object sorted by LastName and
	then asks the User which one he/she wants to delete.
	 */
	void remove(String LastName)
	{
		Scanner in = new Scanner(System.in);
		String Input;
		AddressEntry RemoveMe;
		ArrayList<AddressEntry> Remover = new ArrayList<AddressEntry>();
		Remover = this.find(LastName);
		if(Remover.isEmpty()==false)
		{	
				String regex = "\\d+";
				do
				{
				System.out.println("The following "+ Remover.size()+" entries were found in the address book, select number of entry you wish to remove:");
				for(AddressEntry Address : Remover)
				{
					System.out.println(Remover.indexOf(Address)+". "+Address.toString());
				}
				Input = in.nextLine();
				}while(Input.matches(regex)==false);
				if(Integer.parseInt(Input) < Remover.size() && Integer.parseInt(Input) >= 0)
				{
					System.out.println("Hit y to remove the following entry or n to return to main menu");
					System.out.println(Remover.get(Integer.parseInt(Input)).toString());
					RemoveMe = Remover.get(Integer.parseInt(Input));		
					Input = in.nextLine();
					if(Input.equals("y"))
					{
						for (Iterator<AddressEntry> RemovingIt = AddressBookList.iterator(); RemovingIt.hasNext(); ) 
						{
						    AddressEntry Entry = RemovingIt.next();
						    if (
						    	Entry.getFirst().equals(RemoveMe.getFirst()) &&
						    	Entry.getLast().equals(RemoveMe.getLast()) &&
						    	Entry.getStreet().equals(RemoveMe.getStreet()) &&
						    	Entry.getCity().equals(RemoveMe.getCity()) &&
						    	Entry.getState().equals(RemoveMe.getState()) &&
						    	Entry.getZip().equals(RemoveMe.getZip()) &&
						    	Entry.getEmail().equals(RemoveMe.getEmail()) &&
						    	Entry.getPhone().equals(RemoveMe.getPhone()) 
						    	) 
						    {
						    	RemovingIt.remove();
						    }
						}
					}else
					{	
					System.out.println("No Changes Were Made.");
					}
				}
		} 
	}
	
	
	

	/* 
	Usage: AddressBook.find(LastName)
	Details: Returns an ArrayList of AddressEntry objects containing the String LastName in their AddressEntry.getLast() field. 
	 */
	ArrayList<AddressEntry> find(String LastName)
	{
		ArrayList<AddressEntry> FindResults = new ArrayList<AddressEntry>();
		for(AddressEntry Address : AddressBookList)
		{
				String a = LastName.toLowerCase();
				String b = Address.getLast().toLowerCase();
				if(a.length() <= b.length())
				{
					b = Address.getLast().toLowerCase().substring(0, a.length());
				}
				if(a.equals(b))
				{
					FindResults.add(Address);
				}
				
			
		}
		if(FindResults.isEmpty())
		{
			System.out.println("No Address Found.");	
		}
		return FindResults;
	}
	
	
	
	
	/* 
	Usage: AddressBook.toFile(FileName)
	Details: Saves AddressBook as a File at the path FileName 
	 */
	void toFile(String FileName) throws FileNotFoundException, UnsupportedEncodingException
	{	
		Collections.sort(AddressBookList, new Comparator<AddressEntry>(){
	    public int compare(AddressEntry a,AddressEntry b){
	        return a.getLast().compareTo(b.getLast());
	        // or compare what you want return -1, 0 or 1 
	        // for less than, equal and greater than resp.
	    }
		});
		PrintWriter writer = new PrintWriter(FileName, "UTF-8");		
		for(AddressEntry Address : AddressBookList)
		{
			writer.print(Address.toFile());
		}
		writer.close();
	}
	
	
	/* 
	Usage: AddressBook.getFile(FileName)
	Details: Reads in a file from the path FileName and saves it as a AddressBook Object. 
	 */
	void readFile(String FileName) throws IOException
	{
		if (new File(FileName).exists())
		{
		BufferedReader in = new BufferedReader(new FileReader(FileName)); 
		int LineCount = 0;
		String phone;
		String email="";
		String zip="";
		String state="";
		String city="";
		String street="";
		String last="";
		String first="";
		while (in.ready()) 
		{	
			
			if(LineCount ==0)
			{
				first = in.readLine();
				LineCount++;
			}
			if(LineCount ==1)
			{
				last = in.readLine();
				LineCount++;
			}
			if(LineCount==2)
			{
				street = in.readLine();
				LineCount++; 
			}
			if(LineCount==3)
			{
				city = in.readLine();
				LineCount++;
			}
			if(LineCount==4)
			{
				state = in.readLine();
				LineCount++;
			}
			if(LineCount==5)
			{
				zip = in.readLine();
				LineCount++;
			}
			
			if(LineCount==6)
			{
				email = in.readLine();
				LineCount++;
			
			}
			if(LineCount==7)
			{
				phone = in.readLine();
				AddressEntry newGuy = new AddressEntry(first,last,street,city,state,zip,email,phone);			 		
				AddressBookList.add(newGuy);
				LineCount=0;
			}
	
			
		}
		in.close();
		}
		else
		{
			System.out.println("No Such File Exists.");
		}
	}
	
	

}
